package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.repository.ISessionRepository;
import ru.mtumanov.tm.model.Session;

public class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

}

package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.ISessionRepository;
import ru.mtumanov.tm.api.service.ISessionService;
import ru.mtumanov.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}

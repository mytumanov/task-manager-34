package ru.mtumanov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class FatalSystemException extends AbstractSytemException {

    public FatalSystemException(@NotNull final Throwable cause) {
        super(cause);
    }

}

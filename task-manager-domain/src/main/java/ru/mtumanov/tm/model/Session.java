package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
public class Session extends AbstractUserOwnedModel {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}

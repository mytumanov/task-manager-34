package ru.mtumanov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

@NoArgsConstructor
public final class TaskChangeStatusByIdRs extends AbstractTaskRs {

    public TaskChangeStatusByIdRs(@Nullable final Task task) {
        super(task);
    }

    public TaskChangeStatusByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}
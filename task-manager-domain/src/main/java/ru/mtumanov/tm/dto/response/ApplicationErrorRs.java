package ru.mtumanov.tm.dto.response;

import org.jetbrains.annotations.NotNull;

public class ApplicationErrorRs extends AbstractResultRs {

    public ApplicationErrorRs() {
        setSuccess(false);
    }

    public ApplicationErrorRs(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
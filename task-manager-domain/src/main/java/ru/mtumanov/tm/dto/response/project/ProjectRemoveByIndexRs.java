package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectRemoveByIndexRs extends AbstractProjectRs {

    public ProjectRemoveByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}
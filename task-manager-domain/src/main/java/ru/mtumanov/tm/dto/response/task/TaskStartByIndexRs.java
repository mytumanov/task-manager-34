package ru.mtumanov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

@NoArgsConstructor
public class TaskStartByIndexRs extends AbstractTaskRs {

    public TaskStartByIndexRs(@Nullable final Task task) {
        super(task);
    }

    public TaskStartByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}

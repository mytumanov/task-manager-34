package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultRs;
import ru.mtumanov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRs extends AbstractResultRs {

    @Nullable
    private List<Task> tasks;

    public TaskListRs(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    public TaskListRs(@NotNull final Throwable err) {
        super(err);
    }

}
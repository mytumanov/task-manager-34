package ru.mtumanov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

@NoArgsConstructor
public final class UserLogoutRs extends AbstractResultRs {

    public UserLogoutRs(@NotNull final Throwable err) {
        super(err);
    }

}

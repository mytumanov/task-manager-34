package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectRemoveByIdRs extends AbstractProjectRs {

    public ProjectRemoveByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}
package ru.mtumanov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.User;

@NoArgsConstructor
public final class UserUpdateRs extends AbstractUserRs {

    public UserUpdateRs(@Nullable final User user) {
        super(user);
    }

    public UserUpdateRs(@NotNull final Throwable err) {
        super(err);
    }

}
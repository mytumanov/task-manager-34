package ru.mtumanov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public interface ICommand {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @Nullable
    String getName();

    @Nullable
    Role[] getRoles();

    void execute() throws AbstractException;

}

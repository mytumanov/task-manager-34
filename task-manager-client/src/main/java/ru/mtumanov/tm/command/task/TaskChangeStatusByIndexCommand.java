package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.task.TaskChangeStatusByIndexRq;
import ru.mtumanov.tm.dto.response.task.TaskChangeStatusByIndexRs;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Change task status by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STAUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        if (status == null)
            throw new StatusNotSupportedException();
        @NotNull final TaskChangeStatusByIndexRq request = new TaskChangeStatusByIndexRq(getToken(), index, status);
        @NotNull final TaskChangeStatusByIndexRs response = getTaskEndpoint().taskChangeStatusByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show list arguments";
    }

    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}

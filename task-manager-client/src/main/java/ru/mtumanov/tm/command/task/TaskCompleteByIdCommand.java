package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskCompleteByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskCompleteByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Complete task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRq request = new TaskCompleteByIdRq(getToken(), id);
        @NotNull final TaskCompleteByIdRs response = getTaskEndpoint().taskCompleteById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
